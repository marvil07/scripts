#! /bin/sh -e

device=/dev/ttyUSB0
delay=10

usage() {
    cat <<EOF
$0 ( -i | -x ) [ <csvfile> ]

-r reload pl2313 kernel module before doing anything (requires sudo)
-i import csv into device
-x export device into csv
-f overwrite existing csv file during export
-n simulate, don't do anything but checks
-h this help

<csvfile> defaults to wouxun.csv and must exist for import
EOF
    trap "" 0
    exit 1
}

fail() {
    cat <<EOF
this thing is not working. try this:
 1. plug the damn thing in dude
 2. turn it off, unplug the dongle, plug it back in and turn it back on

if all else fails: try turning it off and on again.

if you already tried that, smartass, try reloading the pl2303 driver

and if that fails too, try again tomorrow
EOF
    trap "" 0
    exit 1
}

do_export() {
    if [ -e "$csv" -a -z $force ]; then
        while true; do
            printf "file $csv already exists, overwrite? [y/N] "
            read ans
            case "$ans" in
                [Yy]|[Yy][Ee][Ss]) break;;
                ""|[Nn]|[Nn][Oo]) trap "" 0; exit 1;;
                *) echo "uh? try again.";;
            esac
        done
    fi
        
    if owx-check -p ${device}; then
        echo "device detected on ${device}, letting it rest $delay seconds"
        $simulate sleep $delay
        echo "fetching the current image in file.bin"
        $simulate owx-get ${force} -p ${device} -o file.bin
        echo "converted to CSV file ${csv}"
        $simulate owx-export -i file.bin -o "${csv}"
        echo "converting back to unix file format"
        $simulate sed -i "s/\r//" "${csv}"
    fi
}

do_import() {

    if [ ! -r "$csv" ]; then
        echo "file $csv not readable"
        usage
    fi

    if owx-check -p ${device}; then
        echo "device detected on ${device}, letting it rest $delay seconds"
        $simulate sleep $delay
        echo "fetching the current image in file.bin"
        $simulate owx-get ${force} -p ${device} -o file.bin
        echo "backing up to backup.bin"
        $simulate cp -f file.bin backup.bin
        echo "patching image with $csv"
        $simulate owx-import -i "$csv" -o file.bin
        echo "sleeping $delay more"
        $simulate sleep $delay
        echo "loading image based on backup"
        $simulate owx-put -p ${device}  -i file.bin -r backup.bin
        echo "letting the device rest $delay more seconds"
        $simulate sleep $delay
        echo "okidou, all seems to have worked all great"
        echo "please unplug that proprietary crap out of this box now"
    else
        fail
    fi
}

do_reload_module() {
	$simulate sudo modprobe -r pl2303
	$simulate sleep 1
	$simulate sudo modprobe pl2303
	$simulate sleep $delay
}

set -- `getopt hxinfr $*`

for i; do
        case "$i" in
              -h) shift; usage;;
              -r) shift; reload="yes";;
              -n) shift; simulate="echo > ";;
              -x) shift; dothis="do_export";;
              -i) shift; dothis="do_import";;
              -f) shift; force="-f";;
              --) shift; break;;
        esac
done

csv=${@:-wouxun.csv}

if [ -z "$dothis" ]; then
    usage
fi

if [ ! -z "$simulate" ]; then
    echo "*** SIMULATION: ECHOING COMMANDS ***"
fi

trap fail 0

if [ ! -z "$reload" ]; then
	echo "Reloading pl2313 kernel module..."
	do_reload_module
fi

echo "preparing to $dothis on device..."
$dothis

trap "" 0
