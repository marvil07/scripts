#! /bin/sh

# here we could also use --no-wait/-n, but this implies that control-x
# # does *not* delete the frame, which is annoying.
#
# but multiple calls to this will send urgency hints to the WM for the
# frames created earlier, which might be annoying. a good way to fix
# that is to use -n in the other frames.
emacsclient --alternate-editor="vim" --create-frame "$@" || vim "$@"
