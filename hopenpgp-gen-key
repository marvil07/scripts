#!/usr/bin/env runhaskell

-- create a key with its own fingerprint as a uidpayload
--
-- written by Clint, I am not worthy

import qualified Crypto.Hash.Algorithms as CHA
import Crypto.Number.Serialize (os2ip)
import qualified Crypto.PubKey.RSA as RSA
import qualified Crypto.PubKey.RSA.PKCS15 as P15
import Codec.Encryption.OpenPGP.Fingerprint (fingerprint, eightOctetKeyID)
import Codec.Encryption.OpenPGP.Serialize ()
import Codec.Encryption.OpenPGP.SerializeForSigs (putKeyforSigning, putUforSigning, putPartialSigforSigning, putSigTrailer)
import Codec.Encryption.OpenPGP.Types
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Data.Binary (put)
import Data.Binary.Put (runPut, Put)
import qualified Data.List.NonEmpty as NE
import qualified Data.Set as S
import qualified Data.Text as T
import Data.Time.Clock.POSIX (getPOSIXTime)
import Data.Text.Prettyprint.Doc (pretty)

main :: IO ()
main = do
    (pub, prv) <- RSA.generate 512 65537
    (sub, ssb) <- RSA.generate 512 65537
    tstamp <- fmap (ThirtyTwoBitTimeStamp . round) getPOSIXTime
    let pkp = PKPayload V4 tstamp 0 RSA (RSAPubKey (RSA_PublicKey pub))
        add16 a b = (+) a (fromIntegral b)
        cksum = BL.foldl add16 0 . runPut $ putAlgoSpecific prv
        ska = SUUnencrypted (RSAPrivateKey (RSA_PrivateKey prv)) cksum
        uidpayload = runPut (sequence_ [putKeyforSigning (PublicKeyPkt pkp), putUforSigning (UserIdPkt (T.pack (show (pretty (fingerprint pkp)))))])
        hsigsubs = [
            SigSubPacket False (SigCreationTime tstamp)
          , SigSubPacket False (Issuer ((\(Right x) -> x) (eightOctetKeyID pkp)))
          , SigSubPacket False (KeyFlags (S.fromList [CertifyKeysKey]))
          ]
        usigsubs = []
        uidsigp = SigV4 PositiveCert RSA SHA512 hsigsubs usigsubs 0 (NE.fromList [MPI 0])
        Right uidsig = P15.sign Nothing (Just CHA.SHA512) prv (BL.toStrict (finalPayload (SignaturePkt uidsigp) uidpayload))
        uidsigp' = SigV4 PositiveCert RSA SHA512 hsigsubs usigsubs (fromIntegral (os2ip (B.take 2 uidsig))) (NE.fromList [MPI (os2ip uidsig)])
        uids = [(T.pack (show (pretty (fingerprint pkp))), [uidsigp'])]
        subpkp = PKPayload V4 tstamp 0 RSA (RSAPubKey (RSA_PublicKey sub))
        subcksum = BL.foldl add16 0 . runPut $ putAlgoSpecific ssb
        subska = SUUnencrypted (RSAPrivateKey (RSA_PrivateKey ssb)) subcksum
        subkeypayload = runPut (sequence_ [putKeyforSigning (PublicKeyPkt pkp), putKeyforSigning (PublicSubkeyPkt subpkp)])
        embhsigsubs = [
            SigSubPacket False (SigCreationTime tstamp)
          , SigSubPacket False (Issuer ((\(Right x) -> x) (eightOctetKeyID subpkp)))
          ]
        embusigsubs = [
          ]
        embsigp = SigV4 PrimaryKeyBindingSig RSA SHA512 embhsigsubs embusigsubs 0 (NE.fromList [MPI 0])
        Right embsig = P15.sign Nothing (Just CHA.SHA512) ssb (BL.toStrict (finalPayload (SignaturePkt embsigp) subkeypayload))
        embsigp' = SigV4 PrimaryKeyBindingSig RSA SHA512 embhsigsubs embusigsubs (fromIntegral (os2ip (B.take 2 embsig))) (NE.fromList [MPI (os2ip embsig)])
        subhsigsubs = [
            SigSubPacket False (SigCreationTime tstamp)
          , SigSubPacket False (Issuer ((\(Right x) -> x) (eightOctetKeyID pkp)))
          , SigSubPacket False (KeyFlags (S.fromList [EncryptStorageKey,EncryptCommunicationsKey]))
          ]
        subusigsubs = [
            SigSubPacket False (EmbeddedSignature (embsigp'))
          ]
        subsigp = SigV4 SubkeyBindingSig RSA SHA512 subhsigsubs subusigsubs 0 (NE.fromList [MPI 0])
        Right subsig = P15.sign Nothing (Just CHA.SHA512) prv (BL.toStrict (finalPayload (SignaturePkt subsigp) subkeypayload))
        subsigp' = SigV4 SubkeyBindingSig RSA SHA512 subhsigsubs subusigsubs (fromIntegral (os2ip (B.take 2 subsig))) (NE.fromList [MPI (os2ip subsig)])
        subs = [(SecretSubkeyPkt subpkp subska,[subsigp'])]
        stk = TK (pkp, Just ska) [] uids [] subs
        ptk = TK (pkp, Nothing) [] uids [] (map (\(x, y) -> (secretSubkeyToPublicSubkey x, y)) subs)
    BL.writeFile "/tmp/unencrypted-secret-key.openpgp" (runPut (put stk))
    BL.writeFile "/tmp/public-key.openpgp" (runPut (put ptk))
    print . pretty . fingerprint $ pkp

secretSubkeyToPublicSubkey :: Pkt -> Pkt
secretSubkeyToPublicSubkey (SecretSubkeyPkt pkp _) = PublicSubkeyPkt pkp

putAlgoSpecific :: RSA.PrivateKey -> Put
putAlgoSpecific (RSA.PrivateKey _ d p q _ _ _) = put (MPI d) >> put (MPI p) >> put (MPI q) >> put (MPI u)
    where
        u = multiplicativeInverse q p

multiplicativeInverse :: Integral a => a -> a -> a
multiplicativeInverse _ 1 = 1
multiplicativeInverse q p = (n * q + 1) `div` p
    where n = p - multiplicativeInverse p (q `mod` p)

finalPayload :: Pkt -> BL.ByteString -> BL.ByteString
finalPayload s pl = BL.concat [pl, sigbit, trailer s]
    where
        sigbit = runPut $ putPartialSigforSigning s
        trailer :: Pkt -> BL.ByteString
        trailer (SignaturePkt SigV4{}) = runPut $ putSigTrailer s
        trailer _ = BL.empty
