#!/usr/bin/python3

# prior art:
#
# https://github.com/porras/dmenu-emoji
#
# shell scripts, hardcodes list, doesn't preview emojis
#
# https://github.com/marty-oehme/bemoji/blob/main/bemoji
#
# shell, downloads list off internet, history
#
# https://github.com/fdw/rofimoji/
#
# too much python code, history, ships its own CSV files

import argparse
import logging
import os
import shlex
import subprocess
from typing import Iterator, Optional


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`.
    """

    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):  # type: ignore[no-untyped-def]
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


class UnicodeData:
    """operations on the UnicodeData.txt file

    The format here is basically CSV AKA (semi-)Colon Separated
    Values. full description of the file is at:
    https://www.unicode.org/L2/L1999/UnicodeData.html
    """

    DATA_FILE = "/usr/share/unicode/UnicodeData.txt"
    BLOCKS_FILE = "/usr/share/unicode/Blocks.txt"

    class Category(str):
        """type-checking for a unicode category

            According to:
            https://www.unicode.org/L2/L1999/UnicodeData.html#General%20Category
            the abbreviation is a two-character string, one uppercase,
            then lowercase. enforce that."""
        def __new__(cls, string):
            if len(string) != 2:
                raise ValueError(
                    "category %r has unexpected length (%s != 2), see https://www.unicode.org/L2/L1999/UnicodeData.html#General%%20Category" %  # noqa: E501
                    (string, len(string)),
                )
            instance = super().__new__(cls, string[0].upper() + string[1].lower())
            return instance

    def load(self) -> Iterator[tuple[int, str, Category]]:
        """parse the unicode data file and list of items to process later"""
        with open(self.DATA_FILE, encoding="utf-8") as fp:
            for line in fp:
                codevalue, text, category, _ = line.strip().split(";", maxsplit=3)
                codeint = int(codevalue, 16)  # decode base 16 representation
                yield codeint, text, UnicodeData.Category(category)

    @staticmethod
    def postprocess(
        lines: Iterator[tuple[int, str, Category]],
        only_category: Optional[Category] = None,
    ) -> bytes:
        generated = []
        for code, text, category in lines:
            if only_category is not None:
                if category != only_category:
                    continue
            try:
                repr = chr(code) + "\t" + text
                generated.append(repr.encode("utf-8"))
            except UnicodeEncodeError:
                pass
        return b"\n".join(generated)

    def parse(self, only_category: Optional[Category] = None) -> bytes:
        return UnicodeData.postprocess(self.load(), only_category)


def main():
    """parse commandline, look for completions, start dmenu and act on the result"""
    logging.basicConfig(level="WARNING", format="%(message)s")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    parser.add_argument(
        "--dmenu",
        default=os.environ.get("DMENU", "fuzzel -p 'unicode search 🔍' --dmenu"),
        help="which dmenu command to run, default: %(default)s",
    )
    parser.add_argument(
        "--type",
        nargs="?",
        const="wtype -",
        help="type the result, by default with %(const)s",
    )
    parser.add_argument(
        "--clipboard",
        nargs="?",
        const="wl-copy",
        help="type the result, by default with %(const)s",
    )
    # TODO: prompt for category first?
    #
    # another way to do this would be to first parse the Blocks.txt
    # database, prompt for a block, then list only the entires in that
    # block
    parser.add_argument(
        "--category",
        type=str,
        help="restrict to the given unicode 'category', try 'So' for emojis",
    )
    args = parser.parse_args()
    if args.category:
        try:
            args.category = UnicodeData.Category(args.category)
        except ValueError as e:
            parser.error(str(e))
    args.dmenu = shlex.split(args.dmenu)
    if args.type:
        args.type = shlex.split(args.type)
    if args.clipboard:
        args.clipboard = shlex.split(args.clipboard)

    logging.info("loading database restricting in category %s", args.category)
    data = UnicodeData().parse(args.category)

    logging.info("sending %d bytes of data to %s", len(data), args.dmenu)
    try:
        dmenu = subprocess.Popen(
            args.dmenu, stdin=subprocess.PIPE, stdout=subprocess.PIPE
        )
        stdout, stderr = dmenu.communicate(input=data)
    except OSError as e:
        logging.error("failed to call %s: %s", args.dmenu, e)
        return
    logging.debug("dmenu returned: %r", stdout)
    symbol, text = stdout.strip().split(b"\t")
    if args.type:
        logging.info("typing result %r (%s) with %s", symbol, text, args.type)
        try:
            wtype = subprocess.Popen(args.type, stdin=subprocess.PIPE)
            wtype.communicate(symbol)
        except OSError as e:
            logging.error("failed to call %s: %s", args.type, e)
            return
    elif args.clipboard:
        logging.info(
            "sending result %r (%s) to clipboard with %s", symbol, text, args.clipboard
        )
        try:
            wtype = subprocess.Popen(args.clipboard, stdin=subprocess.PIPE)
            wtype.communicate(symbol)
        except OSError as e:
            logging.error("failed to call %s: %s", args.clipboard, e)
            return
    else:
        print(symbol.decode("utf-8"), text.decode("utf-8"))


if __name__ == "__main__":
    main()
