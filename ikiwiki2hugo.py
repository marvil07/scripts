#!/usr/bin/python3

"""convert an ikiwiki git repo into hugo"""

# Copyright (C) 2019 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Assertions:
# 1. all path names and file contents can be decoded in the current locale
# 2. only markdown files with a mdwn or md suffix are to be converted
#
# Inspired by https://blog.jak-linux.org/2018/10/25/migrated-website-from-ikiwiki-to-hugo/


import argparse
from glob import glob
import logging
import os
import os.path
import pathlib
import re
import subprocess


def call(args):
    logging.debug("calling: %s", args)
    return subprocess.check_call(args)


def fake_call(args):
    logging.debug("would call: %s", args)


def parse_arguments():
    global call
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--repository", default=os.getcwd())
    parser.add_argument("--dry-run", "-n", action="store_true")
    parser.add_argument(
        "--exclude", "-e", nargs="+", default=[".git", ".ikiwiki", "themes"]
    )
    parser.add_argument(
        "--content-dir",
        default=None,
        help="if we need to move files inside a 'content' directory, provide its name here, default: do not move",
    )
    parser.add_argument("--test", "-t", action="store_true")
    args = parser.parse_args()
    if args.dry_run:
        call = fake_call
    args.repository = pathlib.Path(args.repository)
    return args


def git_move_all(source, destination):
    for path in source.iterdir():
        call(["git", "mv", str(source / path), str(destination / path.name)])


def process_path(path, dryrun, content_dir):
    # XXX: hardcoded (assumption 2)
    if path.suffix in (".mdwn", ):
        logging.debug("fixing suffix %s on %s", path.suffix, path)
        if not dryrun:
            path = fix_suffix(path, dryrun)
    # XXX: hardcoded (assumption 2)
    if path.suffix in (".md", ".mdwn"):
        rewrite_file(path, dryrun, content_dir)
    else:
        logging.info("not touching non-markdown file: %s", str(path))


def fix_suffix(path, dryrun):
    call(["git", "mv", str(path), str(path.with_suffix(".md"))])
    return path.with_suffix(".md")


def rewrite_file(path, dryrun, content_dir):
    tmpfile = path.with_suffix(".md.tmp")
    logging.debug("rewriting source file: %s", path)
    with path.open() as source, tmpfile.open("w") as tmp:
        content = source.read()
        converted = ikiwiki2hugo(content, path, content_dir)
        tmp.write(converted)
    if dryrun:
        tmpfile.unlink()
    else:
        tmpfile.replace(path)
        call(["git", "add", str(path)])


LINK_RE = re.compile(
    r"""
                        \[\[(?=[^!])            # beginning of link
                        (?:
                                ([^\]\|]+)      # 1: link text
                                \|              # followed by '|'
                        )?                      # optional

                        ([^\n\r\]#]+)           # 2: page to link to
                        (?:
                                \#              # '#', beginning of anchor
                                ([^\s\]]+)      # 3: anchor text
                        )?                      # optional

                        \]\]                    # end of link
""",
    re.VERBOSE,
    )
LINK_PIPE_RE = re.compile(r"\[\[(?!!)([^\|\]]+)\|([^]]*)\]\]")
TOC_RE = re.compile(r"\[\[!toc")

# another way of doing this
MACRO_REGEX = re.compile(
    r'''
(\\?)           # 1: escape?
\[\[(!)         # directive open; 2: prefix
([-\w]+)        # 3: command
(               # 4: the parameters..
        \s+     # Must have space if parameters present
        (?:
                (?:[-.\w]+=)?           # named parameter key?
                (?:
                        """.*?"""       # triple-quoted value
                        |
                        "[^"]*?"        # single-quoted value
                        |
                        \'\'\'.*?\'\'\'       # triple-single-quote
                        |
                        <<([a-zA-Z]+)\n # 5: heredoc start
                        (?:.*?)\n\5     # heredoc value
                        |
                        [^"\s\]]+       # unquoted value
                )
                \s*                     # whitespace or end
                                        # of directive
        )
*)?             # 0 or more parameters
\]\]            # directive closed
''',
    re.VERBOSE | re.DOTALL | re.MULTILINE,
)


# # cargo-culted from Ikiwiki.pm, preprocess sub
PARAM_REGEX = re.compile(
    r'''
(?:([-.\w]+)=)?	# 1: named parameter key?
(?:
    """(.*?)"""	# 2: triple-quoted value
|
    "([^"]*?)"      # 3: single-quoted value
|
    \'\'\'(.*?)\'\'\'     # 4: triple-single-quote
|
    <<([a-zA-Z]+)\n # 5: heredoc start
    (.*?)\n\5       # 6: heredoc value
|
    (\S+)           # 7: unquoted value
)
    (?:\s+|$)               # delimiter to next param
''',
    re.VERBOSE | re.DOTALL | re.MULTILINE,
)


def ikiwiki_directives(content, callback=None):
    for match in MACRO_REGEX.finditer(content):
        if match.group(1):  # 1. escape
            continue
        # parse 4. parameters
        if not match.group(4):
            continue
        params = ikiwiki_params(match.group(4))
        # generate a list of command => params tuples
        yield match.group(3), params, match
        if callback:
            callback(match.group(2), params, match)


def ikiwiki_params(content):
    params = {}
    if not content:
        return params
    for param in PARAM_REGEX.finditer(content):
        name = param.group(1)
        if name == "updated":
            name = "lastmod"
        params[name] = (
            param.group(2)
            or param.group(3)
            or param.group(4)
            or param.group(6)
            or param.group(7)
        )
    return params


def find_target(target, page, content_dir):
    r"""find a target near page  with the content_dir root


    This needs to try:

     1. case-insensitiven (e.g. `[[OtherPage]]` and `[[otherpage]]` both
        work) - implemented

     2. subpage lookups (e.g. `[[otherpage]]` in `foo/subpage` will
        look for `foo/subpage/otherpage`, `foo/otherpage`,
        `otherpage`, in order; `[[foo/subpage]]` will find
        `/foo/subpage` from `bar`, instead of the expected
        `bar/foo/subpage` in HTML) - implemented

     3. absolute lookups (prefixed with `/`, e.g. `[[/about]]` links
        to `https://example.com/foo/about` if the wiki is in
        `example.com/foo`, and *not* `https://example.com/about` as
        HTML normally would - probably relevant only for wikis in
        subdirectories) - NOT IMPLEMENTED

     4. userdir lookups (`[[anarcat]]` links to `[[users/anarcat]]` if
        userdir is set to `users`) in some contexts (namely comments,
        recentchanges, but not normal content) - NOT IMPLEMENTED

     5. backslash escapes (`\[[WikiLink]]` is not a link) -
        implemented by the caller (in LINK_RE)

     6. anchor lookups (`[[WikiLink#foo]]`) - implemented by the
        caller (in LINK_RE)

     7. there might be other rules like underscore (`_`) mapping to
        spaces and other funky escape mechanisms - NOT IMPLEMENTED,
        look at IkiWiki::titlepage for those

    """
    if '://' in target:
        # external link
        return target
    # for poor old test suite
    if page is None or content_dir is None:
        return target
    # find the relative path to the current page, without the suffix.
    # this turns, say, foo/bar.md into foo/bar
    #
    # this is also an assertion that the page given is in content_dir
    relpage = page.relative_to(content_dir).with_suffix('')

    # this is a reimplementation of IkiWiki::bestlink, more or less
    cwd = relpage
    while str(cwd) != ".":
        for t in (target, target.lower()):
            guess = (content_dir / cwd / t).with_suffix(".md")
            if guess.exists():
                return str(cwd / target)
        # TODO: lowercase
        # go up a level
        logging.debug("up one level: %s", cwd)
        cwd = cwd.parent
    guess = (content_dir / target).with_suffix(".md")
    if guess.exists():
        return "/" + target

    # target not found, wild guess
    logging.warning("broken link: %s in page %s relpage %s content %s",
                    target, page, relpage, content_dir)
    return target


def ikiwiki_map_sub(match, params):
    if params.get('show'):
        logging.warning(
            "ignoring show= param in directive , not implemented: %s",
            match.group(),
        )
    # default is "all pages in wiki", according to:
    # https://ikiwiki.info/ikiwiki/directive/map/
    logging.warning("checking map directive %s, params: %r", match, params)
    try:
        pagespec, extra = params.get('pages', '*').split(maxsplit=1)
    except ValueError:
        pagespec, extra = params.get('pages', '*'), ''
    logging.warning("pagespec: %s, extra: %s", pagespec, extra)
    if len(extra):
        logging.warning(
            "ignoring extra pagespec, not implemented: %s",
            extra
        )
    if '!' in pagespec or '(' in pagespec:
        logging.warning(
            "special parameters in pagespec not implemented: %s",
            pagespec
        )
        return match.group()

    base_spec = os.path.dirname(pagespec)
    content = r"""<!-- update with `ls -d %s.md | sed 's/.md$//;s/\(.*\)/ * [\1](%s\/\1)/'` -->""" % (pagespec, base_spec)  # noqa: E501
    content += "\n\n"
    for page in sorted(glob(pagespec)):
        page = re.sub(r'\.[^.]+$', '', page)
        base = os.path.basename(page)
        content += " * [%s](%s)\n" % (base, page)
    return content


def ikiwiki_directive_sub(match):
    command = match.group(3)
    params = ikiwiki_params(match.group(4))
    if command == 'meta':
        # processed already
        return ''
    if command == 'toc':
        return '[[_TOC_]]'
    if command == 'map':
        return ikiwiki_map_sub(match, params)

    logging.debug("ignoring directive %s", command)
    return match.group()


def ikiwiki2hugo(content, path, content_dir):
    r'''convert given file content from ikiwiki to hugo

    >>> print(ikiwiki2hugo("foo\n", None, None))
    foo
    <BLANKLINE>
    >>> data = """[[!meta title="foo"]]
    ... [[!meta date="2011-12-22T01:23:20-0500"]]
    ... [[!meta updated="2011-12-22T01:23:21-0500"]]
    ... [[!toc levels=2 startlevels=3]]
    ...
    ... foo [[link]] [[link|target]]
    ... """
    >>> print(ikiwiki2hugo(data, None, None))
    ---
    title: foo
    date: 2011-12-22T01:23:20-0500
    lastmod: 2011-12-22T01:23:21-0500
    ---
    <BLANKLINE>
    [[_TOC_]]
    <BLANKLINE>
    foo [link](link) [link](target)
    <BLANKLINE>
    '''
    metadata = {}
    for command, params, match in ikiwiki_directives(content):
        if command == 'meta':
            for key, val in params.items():
                if key == "updated":
                    key = "lastmod"
                metadata[key] = val
    header = []
    for key, value in metadata.items():
        header.append("{}: {}".format(key, value))
    if header:
        header = "---\n" + "\n".join(header) + "\n---\n\n"
    else:
        header = ""

    def ikiwiki_link_sub(match):
        target = find_target(match.group(2), path, content_dir)
        text = match.group(1) or target
        if text == '_TOC_':
            # special case, idempotency: do not rewrite GitLab _TOC_ macros
            return match.group()
        anchor = ""
        if match.group(3):
            anchor = "#" + match.group(3)
        return f"[{text}]({target}{anchor})"

    content = LINK_RE.sub(ikiwiki_link_sub, content)
    content = MACRO_REGEX.sub(ikiwiki_directive_sub, content)
    return header + content.lstrip()


def main():
    logging.basicConfig(level="WARNING", format="%(levelname)s: %(message)s")
    args = parse_arguments()
    if args.test:
        import doctest

        doctest.testmod()
        return
    logging.warning(
        "bisco rewrote this for Tails, consider using their version instead"
        "see https://bisco.org/notes/converting-ikiwiki-to-hugo/"
    )
    contentdir = args.repository
    if args.content_dir:
        contentdir = args.repository / args.content_dir
        if contentdir.exists():
            logging.info(
                "content dir already created, skipping mass move: %s", contentdir
            )
        else:
            logging.info("creating content dir: %s", contentdir)
            if not args.dry_run:
                contentdir.mkdir()
            logging.info(
                "moving all files from %s to contentdir %s", args.repository, contentdir
            )
            git_move_all(args.repository, contentdir)
    if args.dry_run:
        # we haven't moved files, so try on existing ones
        contentdir = args.repository
    logging.info("walking entire content dir %s", contentdir)
    for root, dirs, files in os.walk(contentdir):
        for excluded_path in args.exclude:
            if excluded_path in dirs:
                logging.info(
                    "skipping ignored path %s", os.path.join(root, excluded_path)
                )
                dirs.remove(excluded_path)
        for path in files:
            full_path = pathlib.Path(root) / path
            process_path(full_path, args.dry_run, contentdir)


if __name__ == "__main__":
    main()
