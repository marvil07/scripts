#! /usr/bin/python

"""%prog [options]

Password dictionnary generator

If you know a bit what your are guessing for in a password, just
brute forcing your way through all the keyspace is not efficient.

This script allows you to focus on some combinations of words, birth
days, and times."""

import math
import sys
import datetime

from optparse import OptionParser

parser = OptionParser(usage=__doc__)
parser.add_option("-f", "--file", dest="wordfile",
                  help="add list of words from FILE", metavar="FILE")
parser.add_option("-w", "--words", dest="words",
                  help="add list of WORDS", metavar="WORDS")
parser.add_option("-s", "--seperator", dest="sep", default=" ",
                  help="words are seperated with SEP", metavar="SEP")
parser.add_option("-m", "--max", dest="max", default=None)
parser.add_option("-o", "--output-seperator", dest="outsep", default=" ",
                  help="output words are seperated with SEP", metavar="SEP")
parser.add_option("-t", "--time", dest="time", default=False, action="store_true",
                  help="add all possible time", metavar="TIME")
parser.add_option("-b", "--birthday", dest="birthday", default=False, action="store_true",
                  help="add all possible birthdays", metavar="TIME")
parser.add_option("-e", "--epoch", dest="epoch", default=1950, type="int",
                  help="first possible birthday YEAR", metavar="YEAR")

(options, args) = parser.parse_args()

if options.words and options.wordfile:
    print "you can't use -w and -f"
    exit()

if options.words:
    words = options.words.split(options.sep)

if options.wordfile:
    words = open(options.wordfile, 'r').read()
    words = words.split("\n")

def fact(n):
    fact = 1
    for i in range(1,n+1):
        fact = fact*i
    return fact

if options.max:
    options.max = int(options.max)

# taken from http://code.activestate.com/help/terms/
# MIT licensed: http://www.opensource.org/licenses/mit-license.php
def all_perms(str):
    if len(str) <=1:
        yield str
    else:
        for perm in all_perms(str[1:]):
            for i in range(len(perm)+1):
                #nb str[0:1] works in both string and list contexts
                yield perm[:i] + str[0:1] + perm[i:]

def all_times():
    perms = []
    for hour in range(23):
        for minute in range(59):
            perms += [ "%s%02d" % ( hour, minute ),
                       "%sh%02d" % ( hour, minute ),
                       "%s:%02d" % ( hour, minute ) ]
    return perms

# this is by no means all dates:
# * we are not using seperators, which can vary a lot
# * we are always zero-filling the days and months
# * we are using 4-digits years
# * we are assuming a gregorian calendar
#
# Additionnally, this has the following problems:
# * it will generate duplicate items (february 2nd and 2nd of ferbruary)
# * it will generate invalid dates (february 30th)
# * it always uses the year
def all_dates():
    perms = []
    for year in range(options.epoch, datetime.date.today().year):
        perms += [ "%04d" % year ]
        for month in range(12):
            perms += [ "%02d%04d" % ( month, year ),
                       "%02d%04d" % ( year, month ) ]
            for day in range(31):
                perms += [ "%02d%02d%04d" % ( day, month, year ),
                           "%02d%02d%04d" % ( month, day, year ),
                           "%04d%02d%02d" % ( year, month, day ),
                           "%04d%02d%02d" % ( year, day, month ),
                           ]
    return perms

if options.time:
    times = all_times()
else:
    times = [""]

if options.birthday:
    dates = all_dates()
else:
    dates = [""]

verif = {}
if words:
    # if we pick only k elements out of the subset of n elements, we
    # have less results (n!/(n-k)! instead of n!)
    if options.max:
        perms = fact(len(words))/fact(len(words)-(options.max))
    else:
        perms = fact(len(words))
    print "choosing %d words out of %d submitted, %d permutations possible" % ( options.max, len(words), perms)
    
    i = 0
    # this is suboptimal: there's an easiest way to pick all k
    # combinations of a subset of n elements without just trimming the
    # list of all permitations of the full n subset, which is much
    # larger (n! > n!/(n-k)!, see above)
    #
    # i am just not smart enough to do this right now
    for p in all_perms(words):
        # specifically, this is here we are being stupid
        p = options.outsep.join(p[:options.max])
        if p in verif:
            continue
        else:
            verif[p] = True
            i += 1
        for t in times:
            for d in dates:
                print p + options.outsep.join(t + d)
    print "generated %d permutations" % i
    if perms == i:
        print "that is the correct number"
    else:
        print "that is %d permutations off" % (i-perms)

else:
    for t in times:
        for d in dates:
            if t != "" and d != "":
                print options.outsep.join([t, d])
            elif t == "":
                print d
            elif d == "":
                print t
            
                
