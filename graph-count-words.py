#!/usr/bin/python3
# coding: utf-8

"""graph word counts over time"""
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
from datetime import datetime
import logging
import os.path
import subprocess
import sys

import matplotlib.pyplot as plt
import pandas as pd


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description=__doc__, epilog="""""")
    parser.add_argument(
        "--verbose",
        "-v",
        dest="log_level",
        action="store_const",
        const="info",
        default="warning",
    )
    parser.add_argument(
        "--debug",
        "-d",
        dest="log_level",
        action="store_const",
        const="debug",
        default="warning",
    )
    parser.add_argument("--target", "-t", help="where word counts live")
    parser.add_argument("--threshold", help="word count difference to report",
                        type=int, default=0)
    parser.add_argument('--output', '-o', type=argparse.FileType('wb'),
                        default=sys.stdout, nargs='?', metavar='FILE',
                        help='save graph to FILE or stdout')
    parser.add_argument('--start', help='restrict start date')
    parser.add_argument('--end', help='restrict end date')
    return parser.parse_args(args=args)


def main(args):
    dates = []
    counts = []
    large_commits = []
    prev_count = 0
    relevant_commits = {
        'b5c36f5f0afe8a7533e8f0bfafb0d596f183e8a0': 'weasel: initial import',
        'ca1d6a3363f4344e4d1ef636a6198a46850da729': 'anarcat: first commit',
        # "6f73bb9917e3d192ec0b7c413296ad8684ee5e92": "anarcat: alberti import",
        # ("anarcat: upgrade docs", pd.to_datetime("2019-03-20"), 25000),
        "998d5e71d93f7c110357036059452f1e3cdaafb9": "anarcat: install procedures",
        "476eba4f346bbd294d08c54243405027ed5506b7": "anarcat: backups",
        "a48b435ad74c95e657308e1295419a2a26f200b2": "anarcat: email",
        "8ef62a90893768ece8bd292a08f19155a7c61106": "pastly: ZNC",
        "e43ee15685dd64e495ac6376a3aacfe4bc884131": "hiro: installation diagrams",
        # "fa455a9acff27e1d1900564943c1401368965ecc": "anarcat: ganeti",
        # "0d400c8cdaf28a95af6071dfdb5bb9788d5c2953": "anarcat: fabric",
        "e30adf0ef94de8da4983583d9cd180397c75e3c2": "irl: metrics",
        "33c5d911e17efeee301a0fad2c9672c35c82104f": "irl: git",
        "f91d64b23ceedf234e0e11495300dfe8320a6837": "anarcat: GitLab, Puppet",
        "ea4c5f8d61c41af7071d9333d49f33d8bbb22e66": "anarcat: GitLab migration",
        "d52f2f3c7b09db9de6fa6b9b499edc6199b5cda7": "anarcat: RT & LDAP",
    }
    annotations = []
    for date, rev, count in sorted(list(walk_files(args.target))):
        if rev in relevant_commits.keys():
            annotations.append((relevant_commits[rev], date, count))
        else:
            logging.debug("%s irrelevant", rev)
        dates.append(date)
        counts.append(count)
        if args.threshold and (count - prev_count) > args.threshold:
            large_commits.append((date, rev, count - prev_count))
        prev_count = count

    df = pd.DataFrame(counts, index=pd.to_datetime(dates), columns=["count"])
    logging.debug("df: %s", df)

    limits = [None, datetime.now()]
    if args.start:
        limits[0] = pd.to_datetime(args.start)
    if args.end:
        limits[1] = pd.to_datetime(args.end)

    plot_records(df, args.output, annotations, limits)
    for date, rev, count in large_commits:
        print("on %s, more than %s words (%s) added..." % (date, args.threshold, count))
        subprocess.run(['git', 'log', '-1', '--stat', rev])


def walk_files(target):
    for root, dirs, files in os.walk(target, onerror=walk_handler):
        for name in files + dirs:
            path = os.path.join(root, name)
            logging.debug("# checking %s", path)
            datestr = name[:25]
            rev = name[26:]
            date = datestr

            with open(path) as fp:
                for line in fp:
                    line = line.strip()
                last_line = line
                count, _ = last_line.split(" ", 2)
            yield date, rev, int(count)


def walk_handler(exc):
    logging.warning("scandir failed on %s: %s", exc.filename, exc)


def plot_records(records, output, annotations, limits):
    fig, ax = plt.subplots()
    fig.set_size_inches(10.5, 8.5)
    plt.plot(records.index, records)
    # https://matplotlib.org/3.1.1/gallery/text_labels_and_annotations/annotation_demo.html
    for text, date, y in annotations:
        ax.annotate(
            text,
            arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"),
            xy=(date, y),
            xycoords="data",
            textcoords="offset points",
            xytext=(-100, 10),
        )
    ax.set_xlabel("date")
    ax.set_ylabel("words")
    plt.xlim(limits)
    ax.set_title("TPA wiki word count over time")
    if output == sys.stdout and \
            ('DISPLAY' in os.environ or sys.stdout.isatty()):
        logging.info("drawing on tty")
        plt.show()
    else:
        logging.info('drawing to file %s', output)
        plt.savefig(output,
                    format=output.name[-3:])


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(format="%(message)s", level=args.log_level.upper())
    main(args)
