#!/usr/bin/python3

'''properly cycle focus through all i3 windows'''

# Requirement: i3-py, install with pip install i3-py.
#
# I really don't understand why this isn't a builtin feature into i3.
#
# It is *really* annoying, coming from other WMs, to not be able to do
# that. i thought `force_focus_wrapping no` would do it, but it
# doesn't.
#
# I have tried i3-cycle, but it doesn't actually cycle through
# containers with "next/prev", only "geographically"
# (up/down/left/right) which is not what i want:
# https://github.com/mota/i3-cycle
#
# similar: https://github.com/budRich/i3ass/tree/master/i3pocus
#
# other possible implementation:
# https://github.com/DavsX/dotfiles/blob/master/bin/i3_cycle_windows
#
# why this is not builtin:
# https://www.reddit.com/r/i3wm/comments/4xiyje/why_is_there_no_focus_next_and_bonus_question/

from glob import glob
import os.path
import sys

# until i3_py is packaged (#913543), give virtualenvs a try
sys.path += glob(os.path.expanduser('~/.virtualenvs/i3_py/lib/python3.*/site-packages/'))

import i3


def focus(increment):
    '''find the next window to focus onto

    cargo-culted from:
    https://faq.i3wm.org/question/389/focus-next-window/%3C/p%3E.html
    '''
    num = i3.filter(i3.get_workspaces(), focused=True)[0]['num']
    ws_nodes = i3.filter(num=num)[0]['nodes'] + i3.filter(num=num)[0]['floating_nodes']
    curr = i3.filter(ws_nodes, focused=True)[0]

    ids = [win['id'] for win in i3.filter(ws_nodes, nodes=[])]

    next_idx = (ids.index(curr['id']) + increment) % len(ids)
    next_id = ids[next_idx]

    i3.focus(con_id=next_id)


def main():
    '''i3 cannot seem to focus properly across containers. this command
    simply flattens the workspaces, containers and everything else and
    allows cycling through all windows, at least in theory. untested
    with floating windows or multiple monitors.
    '''
    if len(sys.argv) < 1:
        sys.exit(__doc__)
    direction = sys.argv[1]
    if direction not in ('next', 'prev'):
        sys.exit('usage: i3-focus [ next | prev ]')
    increment = 1 if direction == 'next' else -1
    focus(increment)


if __name__ == '__main__':
    main()
