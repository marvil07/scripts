#! /usr/bin/perl -w

use CGI qw(:standard);
use Socket;

my $whois_path = "/usr/bin/whois";

sub whois_query_form {

    my $q = shift;

    unless (defined($q)) {
	$q = "";
    }
    return
	start_form,
	"Enter host or IP: ", textfield(-name => 'q', -default => $q),
	submit,
	end_form;

}

print header();

if (param()) {

    my $q = param('q');
    $| = 1;

    print
	start_html('Whois query results'),
	h1("Whois query results for \"$q\""),
	"New query:",
	whois_query_form($q);
    print "<pre>\r\n";

    print "DNS info for $q:\r\n";

    my @hostent = ();

    if ($q =~ /[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/) {
      print "(trying PTR type DNS request)\r\n";
      $ip = inet_aton($q);
      @hostent = gethostbyaddr($ip, AF_INET);
    }
    if (!defined(@hostent)) {
      print "(trying A type DNS request)\r\n";
      @hostent = gethostbyname($q);
    }

    if (!defined(@hostent)) {
      print "No DNS information available for $q ($?)\r\n";
    } else {
      my ($name,$aliases,$addrtype,$length,@addrs) = @hostent;
      foreach $_ (@addrs) {
        $_ = join(".", unpack('C4',$_));
      }
      print "Host name: $name" . ( $aliases ? ", aliases: $aliases" : "").
        "\r\nAddress: " . join (", ", @addrs) . "\r\n";
    }

    print "\r\nWhois Records:\r\n";

    my $pid;
    if ($pid = fork()) { # parent
	if ($pid == -1) {
	    print
		"couldn't fork to create whois process!\r\n",
		"please try again a bit later\r\n";
	} else {
	    if (waitpid($pid, 0) == -1) {
		print "oops, no child\r\n";
	    }
	}
	print "</pre>";
    } else {
	# this will not use the shell
	my @args = ($whois_path, $q);
	exec { $args[0] } @args;
	exit(0);		# NOTREACHED
    }

} else {

    print
	start_html('Whois query form'),
	whois_query_form();

}

print end_html;
