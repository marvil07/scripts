#!/usr/bin/python3 -S

import logging

import subprocess
import sys


#
# our target, beat this:
#
# $ multitime -q -n 100 -s 0 ~/bin/i3-pa-mixer volume mute
# ===> multitime results
# 1: -q /home/anarcat/bin/i3-pa-mixer volume mute
#             Mean        Std.Dev.    Min         Median      Max
# real        0.055       0.009       0.044       0.059       0.074
# user        0.032       0.008       0.013       0.032       0.051
# sys         0.028       0.007       0.014       0.029       0.042


def usage():
    sys.exit("Usage: %s {up|down|mute|mute-mic}" % sys.argv[0])


def main():
    logging.basicConfig(level="DEBUG")
    try:
        command = sys.argv[1]
    except IndexError:
        usage()

    logging.warning("deprecated: use wpctl or pactl directly instead, see https://wiki.archlinux.org/title/WirePlumber#Keyboard_volume_control")
    if command == "volume":
        try:
            command = sys.argv[1]
        except IndexError:
            usage()

    # The @DEFAULT_SINK@ default idea comes from:
    # https://community.frame.work/t/sway-i3-wm-on-framework/11208/15
    # but it's documented in the pactl(1) manpage.
    sink = "@DEFAULT_SINK@"
    source = "@DEFAULT_SOURCE@"
    if command in ("up", "down"):
        if command == "up":
            direction = "+2%"
            verb = "raised"
        else:
            direction = "-2%"
            verb = "lowered"
        subprocess.check_call(["pactl", "--", "set-sink-volume", sink, direction])
        logging.info("%s volume %s on sink %s", verb, direction, sink)
    elif command == "mute":
        if not sink:
            logging.error("no default sink found")
            return 1
        subprocess.check_call(["pactl", "--", "set-sink-mute", sink, "toggle"])
        logging.info("toggled mute on sink %s", sink)
    elif command == "mute-mic":
        if not source:
            logging.error("no default source found")
            return 1
        subprocess.check_call(["pactl", "--", "set-source-mute", source, "toggle"])
        logging.info("toggled mute on source %s", source)
    else:
        usage()


if __name__ == "__main__":
    main()
