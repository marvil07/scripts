#!/bin/sh

# this assumes a remote repository (see BORG_REPO below) was created with
#
# borg init --encryption=repokey $BORG_REPO
#
# "repokey" mode is used because we want to enable bare-bones recovery
set -e

cat <<EOF
offline backup program

thank you for doing your backups, myself.
EOF

if [ $# -lt 1 ]; then
    BORG_REPO=$(grep -h "^ssh://.*/borg-$(hostname)" ~/.config/borg/security/*/location)
    echo "using guessed repository path: $BORG_REPO"
else
    BORG_REPO="$1"
fi

export BORG_REPO

# make sure borg doesn't hang while prompting
export BORG_RELOCATED_REPO_ACCESS_IS_OK=no
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=no
export BORG_RSH='ssh -oBatchMode=yes'

BORG_OPTS='--exclude-caches --keep-exclude-tags --one-file-system'
BORG_OPTS="$BORG_OPTS --exclude-if-present .nobackup "
BORG_OPTS="$BORG_OPTS --verbose --stats --progress"
BORG_OPTS="$BORG_OPTS --compression zstd"

borg create $BORG_OPTS \
     -e /home/anarcat/mp3/ \
     -e /home/anarcat/Music/ \
     -e /home/anarcat/video/ \
     -e /home/anarcat/Photos/ \
     -e /home/anarcat/Photos.local/ \
     -e /home/anarcat/iso/ \
     -e /home/anarcat/isos/ \
     -e /home/anarcat/books \
     -e /home/anarcat/books-incoming \
     -e /home/anarcat/dist \
     -e /home/anarcat/VirtualMachines \
     -e '/home/anarcat/VirtualBox VMs' \
     -e '/home/*/.cache/' \
     -e '*/.Trash-*/' \
     -e '*/.bitcoin/blocks/' \
     -e '*/build-area/*' \
     -e "/var/cache/*" \
     -e "/tmp/*" \
     -e "/var/tmp/*" \
     -e "/var/lib/mlocate/mlocate.db" \
     -e /srv/chroot \
     -e "/var/log/*" \
     ::'{hostname}-{now}' / /boot /usr /var /home

echo "pruning borg archives..."
# keep 10 years, 3 months and all backups in the last 7 days
#
# we can't use -d7 here because we don't backup daily. it would count
# backups going way too far back because it "pushes" the other backup
# types back, as "backups selected by previous rules do not count
# towards those of later rules".
borg prune --stat --list -y 10 -m 3 --keep-within 7d --prefix '{hostname}-'

echo "git annex synchronization..."
sudo -u anarcat /home/anarcat/bin/git-annex-sync-all

cat <<EOF
backups finished, issues remaining: https://anarc.at/services/backup/#offsite-procedures
EOF
